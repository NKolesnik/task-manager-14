package ru.t1consulting.nkolesnik.tm.api.repository;

import ru.t1consulting.nkolesnik.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    Task add(Task task);

    Task remove(Task task);

    Task create(String name);

    Task create(String name, String description);

    List<Task> findAll();

    List<Task> findAll(Comparator comparator);

    Task findOneByIndex(Integer index);

    Task findOneById(String id);

    List<Task> findAllByProjectId(String projectId);

    Task removeById(String id);

    void clear();

    int getSize();

}

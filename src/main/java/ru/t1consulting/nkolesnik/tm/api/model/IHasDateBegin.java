package ru.t1consulting.nkolesnik.tm.api.model;

import java.util.Date;

public interface IHasDateBegin {

    Date getDateBegin();

    void setDateBegin(Date dateBegin);

}

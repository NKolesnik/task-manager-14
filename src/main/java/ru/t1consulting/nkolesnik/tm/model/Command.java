package ru.t1consulting.nkolesnik.tm.model;

public class Command {

    private String name = "";

    private String argument = "";

    private String description = "";

    public Command() {
    }

    public Command(final String name) {
        this.name = name;
    }

    public Command(final String name, final String description) {
        this.name = name;
        this.description = description;
    }

    public Command(final String name, final String argument, final String description) {
        this.name = name;
        this.argument = argument;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(final String argument) {
        this.argument = argument;
    }

    @Override
    public String toString() {
        String result = "";
        if (name != null && !name.isEmpty()) {
            result += name + " : ";
        }
        if (argument != null && !argument.isEmpty()) {
            result += argument + " : ";
        }
        if (description != null && !description.isEmpty()) {
            result += description;
        }
        return result;
    }

}
